<?php 

    namespace Controllers;

    use Medoo\Medoo;
    use Ramsey\Uuid\Uuid;

    class MainController {

        private $database = null;
        private $ip = null;

        public function __construct() 
        {
            $this->getUserIP();
            $this->startSession();
            $this->loadEnv();
            $this->connectToDatabase();
        }

        private function loadEnv()
        {
            $dotenv = \Dotenv\Dotenv::createImmutable(__DIR__ . "/../");
            $dotenv->load();
        }

        private function connectToDatabase()
        {
           $this->database = new Medoo([
                'type' => 'mariadb',
                'host' => $_ENV['DB_HOST'],
                'database' => $_ENV['DB_NAME'],
                'username' => $_ENV['DB_USERNAME'],
                'password' => $_ENV['DB_PASSWORD'],
            ]);
        }

        public function index()
        {
            require(__DIR__ . "/../views/index.php");
        }

        public function admin()
        {
            if($this->isLoggedIn()) {
                $data = $this->database->select("codes", "*");
                require(__DIR__ . "/../views/admin.php");
            } else {
                header('Location: /login');
            }
        }

        public function open($uuid)
        {
            $results = $this->database->select("codes", "*", [
                "uuid" => $uuid
            ]);
            if(count($results) && count($results) == 1) {
                $visits = $results[0]['count'] + 1;
                $link = $results[0]['link'];
                $results = $this->database->update(
                    "codes", 
                    [
                        "count" => $visits
                    ], 
                    [
                        "uuid" => $uuid
                    ]
                );
                header("location: " . $link);
            }

        }

        private function startSession()
        {
            session_start();
        }

        private function isLoggedIn()
        {
            return isset($_SESSION['logged_in']) ? true : false;
        }

        public function login()
        {
            if($this->isLoggedIn()){
                header('Location: /admin');
            } else {
                require(__DIR__ . "/../views/login.php");
            }
        }

        public function verifyLogin()
        {
            if(
                isset($_POST['username'])
                && isset($_POST['password'])
                && $_SERVER['ADMIN_USERNAME'] == $_POST['username']
                && $_SERVER['ADMIN_PASSWORD'] == $_POST['password']
                && ($_SERVER['IP_ADDRESS'] == $this->ip || $this->ip == "127.0.0.1")
            ) {
                $_SESSION['logged_in'] = true;
                header('Location: /admin');
            } else {
                header('Location: /admin');
            }
        }

        public function logout()
        {
            if(isset($_SESSION['logged_in'])) {
                unset($_SESSION['logged_in']);
            }
            header('Location: /login');
        }

        public function add()
        {
            if($this->isLoggedIn()) {
                $this->database->insert("codes", [
                    "uuid" => str_replace("-", "", Uuid::uuid4()->toString() . time()),
                    "link" => $_POST['url'],
                ]);
                header('Location: /admin?added=true');
            } else {
                header('Location: /login');
            }
        }

        public function update()
        {
            if($this->isLoggedIn()) {
                $results = $this->database->update(
                    "codes", 
                    [
                        "uuid" => $_POST['uuid'],
                        "link" => $_POST['link']
                    ], 
                    [
                        "id" => $_POST['id']
                    ]
                );
                header('Location: /admin?updated=true');
            } else {
                header('Location: /login');
            }
        }

        public function delete()
        {
            if($this->isLoggedIn()) {
                $uuid = isset($_POST['uuid']) ? $_POST['uuid'] : false;
                if($uuid) {
                    $this->database->delete(
                        "codes", 
                        [
                            "uuid" => $uuid
                        ]
                    );
                }
                header('Location: /admin?removed=true');
            } else {
                header('Location: /login');
            }
        }

        public function backup()
        {
            if($this->isLoggedIn()) {

                /** Download data from database */
                $data = json_encode($this->database->select("codes", "*"));

                /** Create backup file */
                $name = "backup-" . date("d-m-y-h-i-s") . ".json";
                $backup = fopen(__DIR__ . "/../backups/" . $name, "w") or die("Unable to open file!");
                $data = $data;
                fwrite($backup, $data);
                fclose($backup);

                /** Load its content */
                $content = file_get_contents(__DIR__ . "/../backups/" . $name);

                /** Download it */
                header('Content-disposition: attachment; filename=' . $name);
                header('Content-type: application/json');
                echo $content;

            } else {
                header('Location: /login');
            }
        }

        public function restore()
        {

            /** Delete old records */
            $this->database->delete("codes", "*");
            
            /** Restore from backup */
            foreach (json_decode(file_get_contents($_FILES['backup']['tmp_name'])) as $qrcode) {
                $this->database->insert("codes", [
                    "uuid" => $qrcode->uuid,
                    "link" => $qrcode->link,
                    "count" => $qrcode->count
                ]);
            } 

            /** Redirect back with success */
            header('Location: /admin?restored=true');

        }

        public function getUserIP()
        {
            if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
                    $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
                    $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
            }
            $client  = @$_SERVER['HTTP_CLIENT_IP'];
            $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
            $remote  = $_SERVER['REMOTE_ADDR'];
            if(filter_var($client, FILTER_VALIDATE_IP)) {
                $ip = $client;
            }
            elseif(filter_var($forward, FILTER_VALIDATE_IP)) {
                $ip = $forward;
            }
            else {
                $ip = $remote;
            }
            $this->ip = $ip;
        }

    }

?>