/** QR Codes */
addEventListener('DOMContentLoaded', (event) => {

    /** Catch all image placeholders */
    var images = document.getElementsByClassName("svg-image");

    /** For each of them */
    Array.from(images).forEach(function(image){

        /** Extract data */
        var id = image.getAttribute("id");
        var uuid = image.getAttribute("uuid");

        /** Generate QR Codes */
        document.getElementById(id).appendChild(
            QRCode({
                msg: "https://" + window.location.hostname + "/open/" + uuid, 
                pad: 0
            })
        );

        /** Add download link */
        document.getElementById(id).addEventListener("click", function(){
            downloadSVG(
                this.getAttribute("id"),
                this.getAttribute("uuid")
            );
        });

    });

    function downloadSVG(id, name) {
        const svg = document.getElementById(id).innerHTML;
        const blob = new Blob([svg.toString()]);
        const element = document.createElement("a");
        element.download = name + ".svg";
        element.href = window.URL.createObjectURL(blob);
        element.click();
        element.remove();
    }
    
});

/** Success messages */
addEventListener('DOMContentLoaded', (event) => {
    if(document.getElementsByClassName("success-message").length) {
        Array.from(document.getElementsByClassName("success-message")).forEach(element => {
            setTimeout(function(){
                element.remove();
            }, 2000);
        });
    }
});