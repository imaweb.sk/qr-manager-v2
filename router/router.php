<?php 
    
    require (__DIR__ . "/../controllers/MainController.php");

    class Router {

        private $routes;
        private $router;

        public function __construct()
        {
            $this->routes = [
                "get" => [
                    "/" => "MainController@index",
                    "/open/(\w+)" => "MainController@open",
                    "/admin" => "MainController@admin",
                    "/login" => "MainController@login",
                    "/logout" => "MainController@logout",
                    "/admin/backup" => "MainController@backup",
                ],
                "post" => [
                    "/login" => "MainController@verifyLogin",
                    "/admin/add" => "MainController@add",
                    "/admin/delete" => "MainController@delete",
                    "/admin/update" => "MainController@update",
                    "/admin/restore" => "MainController@restore"
                ]
            ];
            $this->execute();            
        }

        private function execute()
        {
            $this->router = new \Bramus\Router\Router();
            $this->router->setNamespace('\Controllers');
            foreach($this->routes["get"] as $route => $action) {
                $this->router->get($route, $action);
            }
            foreach($this->routes["post"] as $route => $action) {
                $this->router->post($route, $action);
            }
            $this->router->run();
        }

    } new Router();

?>