<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Page title -->
    <title>QR Manager</title>

    <!-- Meta tags -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Favicon -->
    <link rel="icon" type="image/png" href="/favicon.png" />

    <!-- Bootstrap -->
    <link 
        href="/assets/css/bootstrap.min.css" 
        rel="stylesheet" 
        integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" 
        crossorigin="anonymous"
    >

    <!-- Custom stylesheet -->
    <link 
        href="/assets/css/style.css" 
        rel="stylesheet" 
    >
    
</head>
<body class="body-index">
    <form action="/login" method="POST" class="w-50">
        <div class="row">
            <div class="col-6 mx-auto">
                <div class="mb-2">
                    <b>QR Manager</b> | Admin panel
                </div>
                <input 
                    type="text" 
                    placeholder="Username"
                    name="username"
                    class="form-control mb-2"
                    autocomplete="off"
                    required
                >
                <input 
                    type="text" 
                    placeholder="Password"
                    name="password"
                    class="form-control mb-3"
                    autocomplete="off"
                    onkeydown="event.target.type = 'password'"
                    required
                >
                <button
                    class="btn btn-primary"
                >
                    Log in
                </button>
            </div>
        </div>
    </form>
</body>
</html>