<!DOCTYPE html>
<html lang="en">
<head>

    <!-- Page title -->
    <title>QR Manager</title>

    <!-- Meta tags -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Manifest -->
	<link rel="manifest" href="/manifest.json">

    <!-- Favicon -->
    <link rel="icon" type="image/png" href="/favicon.png" />

    <!-- Bootstrap -->
    <link 
        href="/assets/css/bootstrap.min.css" 
        rel="stylesheet" 
        integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" 
        crossorigin="anonymous"
    >

    <!-- Custom stylesheet -->
    <link 
        href="/assets/css/style.css" 
        rel="stylesheet" 
    >
    
</head>
<body class="body-admin">

    <!-- Navigation -->
    <div class="nav border-bottom pt-3 pb-3 bg-white">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-4 text-center text-md-start mb-3 mb-md-0">
                    <b>QR Manager</b>
                </div>
                <div class="col-12 col-md-8 text-center text-md-end">
                    <a 
                        href="/admin/backup"
                        class="text-decoration-none text-black me-3"
                    >
                        Backup
                    </a>
                    <span 
                        class="text-decoration-none text-black me-3 restore-btn"
                        onclick="document.getElementsByClassName('restore')[0].style.display = 'block';"
                    >
                        Restore
                    </span>
                    <a 
                        href="https://pageloot.com/qr-code-scanner"
                        class="text-decoration-none text-black me-3"
                        target="_blank"
                    >
                        Scanner
                    </a>
                    <a 
                        href="/logout"
                        class="text-decoration-none text-black"
                    >
                        Logout
                    </a>
                </div>
            </div>
        </div>
    </div>

    <!-- PWA Install Button -->
    <button
        id="install-btn"
        class="d-none"
    >
        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="white" class="bi bi-window-plus" viewBox="0 0 16 16">
            <path d="M2.5 5a.5.5 0 1 0 0-1 .5.5 0 0 0 0 1ZM4 5a.5.5 0 1 0 0-1 .5.5 0 0 0 0 1Zm2-.5a.5.5 0 1 1-1 0 .5.5 0 0 1 1 0Z"/>
            <path d="M0 4a2 2 0 0 1 2-2h11a2 2 0 0 1 2 2v4a.5.5 0 0 1-1 0V7H1v5a1 1 0 0 0 1 1h5.5a.5.5 0 0 1 0 1H2a2 2 0 0 1-2-2V4Zm1 2h13V4a1 1 0 0 0-1-1H2a1 1 0 0 0-1 1v2Z"/>
            <path d="M16 12.5a3.5 3.5 0 1 1-7 0 3.5 3.5 0 0 1 7 0Zm-3.5-2a.5.5 0 0 0-.5.5v1h-1a.5.5 0 0 0 0 1h1v1a.5.5 0 0 0 1 0v-1h1a.5.5 0 0 0 0-1h-1v-1a.5.5 0 0 0-.5-.5Z"/>
        </svg>
    </button>

    <!-- Register service worker -->
	<script>
    
		navigator.serviceWorker.register('/sw.js', { scope: '/' }).then(function (registration) {

			window.addEventListener('beforeinstallprompt', (e) => {

				// Prevent showing install bar on mobile
				e.preventDefault();
				differedPrompt = e;

				// Handle install button
				document.getElementById("install-btn").classList.remove("d-none");
				document.getElementById("install-btn").addEventListener("click", () => {
					differedPrompt.prompt();
				});

			});

			// Hadle post-installation
			window.addEventListener('appinstalled', (evt) => {
				document.getElementById("install-btn").classList.add("d-none");
			});

		}).catch(function (e) {
			console.error('Error during service worker registration:', e);
		});
	</script>

    <!-- Main content -->
    <main class="mt-3 mb-3">

        <!-- Restore button -->
        <div class="restore mb-3">
            <div class="container">
                <div class="card">
                    <div class="card-header bg-white p-3">
                        Restore from backup
                    </div>
                    <div class="card-body">
                        <form 
                            action="/admin/restore" 
                            method="POST" 
                            enctype='multipart/form-data'
                        >
                            <div class="row">
                                <div class="col-12 col-md-10 mb-2 mb-md-0 col-xl-10">
                                    <input 
                                        type="file" 
                                        name="backup" 
                                        class="form-control"
                                    >
                                </div>
                                <div class="col-12 col-md-2 col-xl-2">
                                    <button
                                        class="btn btn-primary w-100"
                                    >
                                        Restore
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- Actual content -->
        <div class="container">
            <div class="card">
                <div class="card-header bg-white p-3">
                    <form action="/admin/add" method="post">
                        <div class="row">
                            <div class="col-12 col-md-10 mb-2 mb-md-0 col-xl-11">
                                <input 
                                    type="url" 
                                    placeholder="Insert url"
                                    name="url"
                                    class="form-control"
                                    autocomplete="off"
                                    required
                                >
                            </div>
                            <div class="col-12 col-md-2 col-xl-1">
                                <button
                                    class="btn btn-primary w-100"
                                >
                                    Add
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <?php if(isset($_GET['removed'])) { ?>
                    <div class="border-bottom p-3 bg-success text-white success-message">
                        QR Code has been removed from the system
                    </div>
                    <script>
                        window.history.pushState('removed', 'QR Manager', '/admin');
                    </script>
                <?php } ?>
                <?php if(isset($_GET['added'])) { ?>
                    <div class="border-bottom p-3 bg-success text-white success-message">
                        New QR Code has been created
                    </div>
                    <script>
                        window.history.pushState('added', 'QR Manager', '/admin');
                    </script>
                <?php } ?>
                <?php if(isset($_GET['updated'])) { ?>
                    <div class="border-bottom p-3 bg-success text-white success-message">
                        QR Code has been updated
                    </div>
                    <script>
                        window.history.pushState('updated', 'QR Manager', '/admin');
                    </script>
                <?php } ?>
                <?php if(isset($_GET['restored'])) { ?>
                    <div class="border-bottom p-3 bg-success text-white success-message">
                        QR codes have been restored from the backup
                    </div>
                    <script>
                        window.history.pushState('restored', 'QR Manager', '/admin');
                    </script>
                <?php } ?>
                <div class="card-body p-0">
                    <?php if(count($data)) { ?>
                        <?php foreach($data as $key => $link) { ?>
                            <div class="row m-0 <?php if($key != count($data) - 1) { ?> border-bottom <?php } ?>">
                                <div class="col-12 col-md-3 col-lg-2 p-3 pb-0 pb-md-3">
                                    <div 
                                        id="qr-<?= $key ?>"
                                        class="svg-image"
                                        uuid="<?= $link['uuid'] ?>"
                                        link="<?= $link['link'] ?>"
                                    ></div>
                                </div>
                                <div class="col-12 col-md-9 col-lg-10 p-3 border-start border-md-none">
                                    <div class="row">
                                        <div class="col-12">
                                            <form action="/admin/update" method="post">
                                                <div class="form-floating">
                                                    <input 
                                                        type="number" 
                                                        value="<?= $link['id'] ?>"
                                                        name="id"
                                                        hidden
                                                    >
                                                    <input 
                                                        id="uuid-<?= $key ?>"
                                                        type="text" 
                                                        class="form-control mb-2" 
                                                        placeholder="Insert UUID" 
                                                        value="<?= $link['uuid'] ?>"
                                                        name="uuid"
                                                    >
                                                    <label 
                                                        for="uuid-<?= $key ?>"
                                                    >
                                                        UUID
                                                    </label>
                                                </div>
                                                <div class="form-floating">
                                                    <input 
                                                        id="link-<?= $key ?>"
                                                        type="text" 
                                                        class="form-control mb-2 mb-md-3" 
                                                        placeholder="Insert link" 
                                                        value="<?= $link['link'] ?>"
                                                        name="link"
                                                        autocomplete="off"
                                                    >
                                                    <label 
                                                        for="link-<?= $key ?>"
                                                    >
                                                        Link
                                                    </label>
                                                </div>
                                                <button
                                                    id="submit-<?= $key ?>"
                                                    hidden
                                                >
                                                    Update
                                                </button>
                                            </form>
                                        </div>
                                        <div class="col-12">
                                            <button
                                                class="btn btn-primary me-1 w-100 me-1 mb-2 mb-md-0 w-md-auto"
                                                onclick="document.getElementById('submit-<?= $key ?>').click()"
                                            >
                                                Update
                                            </button>
                                            <form 
                                                action="/admin/delete" 
                                                method="post"
                                                class="d-inline-block w-100 w-md-auto"
                                            >
                                                <input 
                                                    type="text"
                                                    name="uuid"
                                                    value="<?= $link['uuid'] ?>"
                                                    hidden
                                                >
                                                <button
                                                    class="btn btn-dark w-100 w-md-auto"
                                                    type="submit"
                                                    onclick="if(!confirm('Are you sure?')) { return false; }"
                                                >
                                                    Delete
                                                </button>
                                            </form>
                                            <span
                                                class="d-inline-block ps-2 text-center w-100 w-md-auto mt-3 mt-md-0"
                                            >
                                                Visited <?= $link['count'] ?> <?php if($link['count'] == 1) { ?> time <?php } else { ?> times <?php } ?>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } else { ?>
                        <div class="p-3">
                            <p class="m-0">No QR codes available</p>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </main>
   
    <!-- Bootstrap -->
    <script 
        src="/assets/js/bootstrap.min.js" 
    ></script>

    <!-- QR Code library -->
    <script
        src="/assets/js/qrcode.min.js"
    >
    </script>
    
    <!-- Custom scripts -->
    <script
        src="/assets/js/scripts.js"
    ></script>

</body>
</html>